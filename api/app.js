"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var constants = require('./api/models/constants');
var searchController = require('./api/controllers/searchController');
var buildJsonWithOthersJson_1 = require("./api/services/workers/buildJsonWithOthersJson");
var fs = require('fs');
var app = express();
var port = process.env.PORT || constants.port;
var number = 0;
//static files
app.use('/', express.static(constants.public_folder));
//fire controllers
searchController(app);
app.listen(port);
console.log(constants.app_name + ' RESTful API server started on: ' + port);
console.log("script start");
fs.watch('/data/content/', { persistent: true, recursive: true, encoding: 'utf-8' }, function (eventType, filename) {
    console.log('----------------');
    console.log('watcher is running');
    console.log('----------------');
    console.log(eventType);
    if (filename != 'olip_index.json' && filename) {
        console.log(filename + ' has changed');
        resolve();
    }
});
function resolve() {
    var json = new buildJsonWithOthersJson_1.buildJsonWithOthersJson("/data/content", "olip_index.json");
    console.time('test');
    json.start();
    console.timeEnd("test");
}
resolve();
//for testing
module.exports = app;
