#!/bin/sh

# Link to themes and config file
ln -fs /hugo-config/config.toml  /data/config.toml
ln -fs /hugo-config/themes/ /data/themes
# Enable search page

mkdir -p /data/content/search/
ln -fs /hugo-config/_index.md /data/content/search/_index.md
if ! [ -e "/data/index.html" ]; then 
    /usr/local/bin/hugo --templateMetrics  --debug --config /data/config.toml -d /data/
fi

exec "$@"
