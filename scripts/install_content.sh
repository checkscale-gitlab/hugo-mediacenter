#!/bin/bash

function hugo_build()
{
    cd /data/
    echo "[+] Package $1 has been added!"
    echo "[+] Rebuild mediacenter"
    time /usr/local/bin/hugo --templateMetrics  --debug --config /data/config.toml -d /data/
}

[ -d /data/$1 ] || hugo_build
