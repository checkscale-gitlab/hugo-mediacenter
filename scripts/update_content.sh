#!/bin/bash

echo "[+] Package $1 has been updated!"
cd /data/
echo "[+]  Rebuild mediacenter"
time /usr/local/bin/hugo --templateMetrics  --debug --config /data/config.toml -d /data/
