FROM --platform=$TARGETPLATFORM offlineinternet/olip-base:latest

RUN ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
	case $ARCH in \
		armhf) node_build='https://nodejs.org/dist/v13.8.0/node-v13.8.0-linux-armv7l.tar.gz' ;; \
		amd64) node_build='https://nodejs.org/dist/v13.8.0/node-v13.8.0-linux-x64.tar.gz' ;; \
		i386) node_build='https://unofficial-builds.nodejs.org/download/release/v13.8.0/node-v13.8.0-linux-x86.tar.gz' ;; \
		arm64) node_build='https://nodejs.org/dist/v13.8.0/node-v13.8.0-linux-arm64.tar.gz' ;; \
		*) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
	esac && \
	wget --quiet $node_build -O node.tar.gz && \
	tar -C /usr/local -xzf node.tar.gz --strip=1

RUN set -eux; \
	ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	if [[ "$ARCH" = "armhf" ]]; then \
		apt update; \
		apt install -y libatomic1 ; \
	fi

ENV HUGO_BASE_URL http://localhost:1313

WORKDIR /hugo

COPY api ./api
COPY api/app.js ./

COPY package.json ./

RUN ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
	case $ARCH in \
		armhf) arch='ARM' ;; \
		amd64) arch='64bit' ;; \
		i386) arch='32bit' ;; \
		arm64) arch='ARM64' ;; \
		*) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
	esac && \
	wget --quiet -O /tmp/hugo.tar.gz "https://github.com/gohugoio/hugo/releases/download/v0.58.3/hugo_0.58.3_Linux-$arch.tar.gz" && \
	tar xzvf /tmp/hugo.tar.gz -C /usr/local/bin hugo && \
	rm -f /tmp/hugo.tar.gz && \
	chmod +x /usr/local/bin/hugo && \
	npm install

COPY entrypoint.sh /
COPY hugo-config /hugo-config

ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

ADD scripts/* /usr/local/bin/

EXPOSE 3000

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/usr/bin/supervisord"]
