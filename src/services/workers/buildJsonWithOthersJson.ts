var fs = require('fs');


export class buildJsonWithOthersJson {
    path:string;
    descriptor:string
    constructor(path:string, descriptor:string){
        this.path = path;
        this.descriptor = descriptor
    }
    private readDirectory():any{
      return fs.readdirSync(this.path);
    }
    private readFilesInAllDirectory(files:any):string{
      let source:string = null;
        for(let file in files){
            let pathFinal= this.path + '/' + files[file];
            if(fs.lstatSync(pathFinal).isDirectory())
              source += fs.readdirSync(pathFinal);
        }
        return source;
    }
    private strcmp(search, string){
      return((search == string) ? 0 :(( search > string) ? 1 : -1));
    }    
    private checkForJsonInSources(sources:string):string{
      let json:string;
      let object = sources.split(",");
      let pattern = 'index.json'
      for( let value in object){
         if(this.strcmp(object[value], pattern) == 0){
          json = object[value]
         }
      }
      return "/" + json;
    }
    private writeAJsonFileFromAllFolders(json:string, files:any, objects:any):string{
      json = "/index.json";
      let tmp;
      let output = " ";
      var final:string = " ";
      for(let object in objects){
        tmp = this.path + '/' + objects[object] + json;
          if(fs.existsSync(tmp)){
              final  += output + fs.readFileSync(tmp, "utf-8")
              }
      }
      return final;
  }
  private jsonValidator(object:string):string{
    let regex:RegExp = /\] \[/g
    object = object.replace(regex, ',');
    return object
  }
  private buildFinalObject(object:string):any{
    let finalPath = this.path + "/" + this.descriptor;
    return fs.writeFileSync(finalPath, object, 'utf-8');
  }
  public start():any{
      if(fs.existsSync(this.path)){ 
        let files = this.readDirectory();
        let dataInFiles = this.readFilesInAllDirectory(files);
        let getJsonObject = this.checkForJsonInSources(dataInFiles);
        let buildJson = this.writeAJsonFileFromAllFolders(getJsonObject, dataInFiles, files );
        let result = this.jsonValidator(buildJson);
        var objectFinal = this.buildFinalObject(result);
        fs.chmod(this.path + '/' + this.descriptor, '0777', function(err, data){
          if(err)
            console.log(err)
          else 
            console.log("chmod set")
        } )
      }
      else
        console.log('no data');
    
      if(!(fs.existsSync(this.path + '/' + this.descriptor))){
        fs.openSync(this.path + '/' + this.descriptor, 'w')
        fs.chmod(this.path + '/' + this.descriptor, '0777', function(err, data){
          if(err)
            console.log(err)
          else 
            console.log("chmod set")
        } )
      }
        return objectFinal
  }
}
